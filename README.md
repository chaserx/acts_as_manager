# Acts as Manager

Hi, I'm Chase 👋 and this is document about me 👨‍💻.

## Preface

[Everything is in draft](https://about.gitlab.com/handbook/values/#everything-is-in-draft) including this document. If you notice that something is not right, please propose an MR to update it.

## Introduction

My name is Chase Southard (sounds like Suh'thûrd).

As an experienced Engineering Manager with over 7 years of leadership experience, I've led teams at GitLab and served as VP of Engineering at various startups. At GitLab, I managed the Fulfillment team within the Utilization Group and previously led the Progressive Delivery team in the CI/CD department. Prior to GitLab, I held Director and VP of Engineering roles at startups across Kentucky and Maryland.

![🔊 Pronunciation of Southard](./southard.mp3)

## Recent roles and where to find me

🧑‍🔬 Engineering Manager, GitLab, Fulfillment, [Utilization](https://about.gitlab.com/handbook/engineering/development/fulfillment/utilization/)

🦊 [My professional GitLab profile, csouthard](https://gitlab.com/csouthard)

🦊 [My personal GitLab profile, chaserx](https://gitlab.com/chaserx)

🐙 [My personal GitHub profile, chaserx](https://github.com/chaserx)

## Management focus

I tend to have a _default_ focus that is split like this: 80% People, 10% Process, and 10% Product.

I thrive on close partnerships with Product leaders, creating an effective bridge between product vision and technical execution. My approach emphasizes lean, purposeful processes that drive results while maintaining team agility[^1].

## Values, observations, and other deep thoughts

- Strive to continuously move the team forward and get shit done.
- We are fully responsible for everything in our world. I own what is in my immediate sphere of influence.
- At my core, I want to be helpful. Being in service to others is the common denominator in everything that I do.
- Our processes should have a defined outcome like understanding team velocity, effective planning, or helping us manage up.
- Communication can be difficult, but effective communication is a key component to team alignment and positive outcomes.
- Default to being fully transparent whenever possible.
- Everyone has a seat at the table.
- Happy teams are more productive.
- Stress makes you do dumb things.
- The people side software development is more critical than the technology employed.
- Helping each other, pair programming, teaching, and giving feedback are more important to me than any algorithm.
- Start small. Minimum Viable Everything.

## My top strength

As an engineering leader, I excel at building high-performing teams through:

- Fostering psychological safety and inclusive team cultures where innovation thrives
- Accelerating career growth through mentorship and actionable feedback
- Partnering with Product to align technical strategy with business objectives
- Attracting and retaining exceptional engineering talent (this is likely you reading this 👋)
- Driving data-informed decisions through meaningful metrics and process optimization
- Providing strategic context to empower autonomous decision-making
- Building cross-functional partnerships to achieve organizational goals

## Areas for mindful attention

- Balancing empathy with accountability
- Maintaining strategic perspective while handling details
- Identifying and addressing blind spots
- Avoiding decision paralysis
- Managing scope creep and over-commitment
- Preventing burnout (both personal and team)
- Balancing urgent vs. important work
- Addressing conflicts early rather than hoping they resolve themselves
- Maintaining healthy boundaries while being supportive

## How to communicate with me

- I'm generally working 8:30am - 6pm US Eastern time. I frequently break my day up and stretch it out.
- I'm always available for coffee chats. I find both management and remote work to be a bit lonely. Don't be shy, please add a coffee chat to my calendar. I want to learn more about you and your part of the world.
- I tend to look at Slack before other places. Please mention me there and in issues or merge requests.
  - At the beginning of the day and a couple of times thereafter, I look for Async communication in this order: Slack, GitLab TODOs, then Email. Email gets of out hand very quickly. If you're waiting on feedback from me, please mention me in an Issue or a Merge Request.
- My personal cell phone number is in my Slack profile.
  - You are welcome to text or call especially if it's an emergency or another serious situation. Note: that I will have [do not disturb functions](https://support.apple.com/en-us/HT204321#schedule) enabled after 9pm US Eastern Time, and I may not pickup the phone if I don't recognize the number, just call back within 3 minutes to get through.

## How I approach my job

### Technical credibility

My leadership style emphasizes empowering engineers to own the critical path while I focus on removing obstacles and improving team efficiency. I provide technical guidance through code reviews and architectural discussions, serving as a trusted advisor to the team. With broad technical experience across multiple technologies, I specialize in web application development, particularly with Ruby on Rails.

I keep my technical skills sharp with small personal projects, process automation, and volunteer work.

### Leadership credibility

I aspire to be a good servant leader. I do **not** want to be an authoritarian boss. I do **not** want to micro-manage anyone - it's a waste of my time and/or yours. Instead, I want to be a facilitator. I want to remove roadblocks, coordinate our efforts, and, on occasion, provide opinions. Ideally, I'm making it easier for you to get your work done and to move the entire team along.

If I am time-constrained while working on something that is high priority, I may ask for updates. Generally, it's better to be transparent with your async updates when telegraphing information to me. In short, I like it when you manage up.

I think if I were better at sports, I might have been a [coach](https://www.entrepreneur.com/article/360155). Coaches are on the sidelines. On occasion, they huddle with the team to talk about their next moves, but they're not in the action on the field. They help the team make a plan. They let the team execute, and, later they facilitate a retrospective of the game. The team is responsible for executing on the plan and adjusting to new input. Each team member has a role and, as a unit, they support each other to achieve success. This is how I think about software teams too. I purposefully avoid writing or reviewing code - I'm not on the field. This doesn't mean you can't ask for my opinion though. I'm here to help the team [observe, orient, decide, and act](https://thestrategybridge.org/the-bridge/2020/3/17/the-ooda-loop-and-the-half-beat) on our next moves. As such, **encouragement, humility, and trust are very important to me** just like Coach Wooden.

### 1-on-1s

I hold weekly one-on-ones with all of my direct reports - it is truly the *best part of my job*. I really enjoy these interactions. I learn something new every time. I've been using this [template](https://docs.google.com/document/d/15qt4E3P4eGc_FqWj52luAb7JLDu_fug4CStxXrUuKHg/edit?usp=sharing) for a while and I think it works fine. I'm happy to try things, so please don't feel committed to this format.

One-on-ones are a place where *you to set the agenda*. What is going well? What could be better? Want to just kvetch about something? What can I do to help you? Would you like to not talk about work for 30 min, that's fine too. See also the Managing Up section below.

By default, 1-on-1s are a 30 minute conversation. I'll schedule the first meeting, but you can always suggest a time and duration that works for you.

### Communication style

I excel at written communication and enjoy thoughtful discourse. While I can be enthusiastic and talkative in conversations, I sometimes need time to organize my thoughts before speaking. I value clear communication and welcome requests for clarification – both giving and receiving. At any point, please ask for clarification when I'm not making sense, I'll try to do the same.

### Delivering feedback

I love feedback. I am constantly trying to get better. **If something is wrong, please say so. I will thank you for it.**

My feedback style balances typical Midwesterner politeness with [Radical Candor](https://www.radicalcandor.com) principles and techniques from [Crucial Conversations](https://cruciallearning.com/books/). I strive to deliver feedback that is both caring personally and challenging directly, creating psychological safety through mutual purpose and mutual respect. While my natural empathy helps build strong relationships, I've learned to pair it with direct, actionable feedback to drive growth and development. By focusing on facts, sharing stories, and encouraging dialogue, I aim to make feedback both kind and impactful while maintaining a safe space for open discussion.

I don't expect you deliver feedback in exactly the same manner that Ii do, but I do want your feedback to be grounded in some principles.

- Behavior based, not person based.
- Grounded in reality including specific details.
- Results oriented. What outcome can we achieve together?

I will likely use a feedback framework like `please keep doing X; please stop doing y`. There's a spot in my preferred 1-on-1 template for feedback, please use it.

## Managing up

Managing up is one of those skills that is really important. As a manager, I like it when I'm managed up, and I try to be better at managing up myself. Managing up is one of those things that can help even if you don't have positive working relationship with your manager. Your boss likely has shortcomings (👋 Hi). Part of being an effective team member is to work with those shortcomings; helping to make that relationship work. 

You don't need to anticipate my every need, but I do need you to help me with my blind spots. Bringing things to my attention publicly or privately is one of the best actions you can do. Practically, this means adding to 1-on-1 agendas, raising concerns early in issues or in Slack, contributing to retrospective issues, giving me feedback, and similar activities. Managing up can be difficult. Below are a list of tips that might be helpful.

- [30 Tips for Managing Up | First Round Review](https://firstround.com/review/a-tactical-guide-to-managing-up-30-tips-from-the-smartest-people-we-know/)
- [What Does It Mean to Manage Up?](https://www.tinypulse.com/blog/what-does-it-mean-to-manage-up)
- [What Everyone Should Know About Managing Up](https://hbr.org/2015/01/what-everyone-should-know-about-managing-up)
- [Guide to managing up: What it means and why it’s important](https://www.cultureamp.com/blog/managing-up-importance)

## Background

- I'm from [Louisville, Kentucky](https://en.wikipedia.org/wiki/Louisville,_Kentucky), USA. I currently live in [Lexington, Kentucky](https://en.wikipedia.org/wiki/Lexington,_Kentucky) both cities are famous for their horses and [bourbon](https://kybourbontrail.com/about/) - I'm allergic to only one of those.
- I studied biological sciences at university. I graduated with a bachelor of science degree in biology from the University of Kentucky, with a focus on experimental neurobiology. If you want to really get nerdy, here are some [publications](http://www.ncbi.nlm.nih.gov/pubmed/?term=Southard%20RC[au]). But knowing what I know now, I really should have majored in statistics (data science wasn't an option back then) or computer science.
- After college, I spent 10 years working in a cancer research laboratory where I contributed to laboratory operations, scientific inquiry, statistical analysis, building data pipelines, bioinformatics, and software development. It's during this period that I found the Ruby programming language and my life changed forever.
- I have spent 12+ years working for software companies, mostly early stage startups.
- I have spent the last 7+ years in engineering management as a team lead, an engineering manager, a director of development, and as a VP of Engineering.
- I think that Managers should not write production code (at least for product features). So instead, I have volunteered my time with [Code for America](https://www.codeforamerica.org/), [OpenLexington](https://github.com/openlexington), and [Ruby for Good](https://rubyforgood.org/) by contributing to software projects that help the communities in which they serve.
- I have been an [Engineering Manager](https://handbook.gitlab.com/job-families/engineering/development/management/engineering-manager/) at GitLab since January 2020 leading teams in the Ops (CI/CD) and Fulfillment sub-departments.

## Characteristics

- Thanks to childhood illness, I don't hear very well. I might miss something in conversation. Sorry in advance for asking you to repeat yourself.
- Burnout and mental health are a top priority for me. I've been burned out before, and it's not good for me, you, or the company. In an all-remote environment, I tend to work long days covering timezones with team members. Taking time to recharge is important. I am recharged by nature, twice daily walks around the neighborhood with the dogs are a part of my schedule. While walking, I will not respond unless it is an urgent request (Pager Duty, etc).
- I am often the last person to speak in a discussion. I like to take in information, analyze it, and internalize it before I provide an opinion. I will ask questions if I don't understand. This does not mean that I'm despondent and not paying attention. This behavior will become more pronounced as the power distance in the room shifts higher or as the meeting size increases.

### Personality Tests

- Meyers-Briggs Type Indicator: In 2022, [INFP](https://en.wikipedia.org/wiki/Myers–Briggs_Type_Indicator) - Mediator personality. "Someone who possesses the Introverted, Intuitive, Feeling, and Prospecting personality traits. These rare personality types tend to be quiet, open-minded, and imaginative, and they apply a caring and creative approach to everything they do."
- [Social Style](https://about.gitlab.com/handbook/leadership/emotional-intelligence/social-styles/) - Analytical style, on the extreme left line between analytical and amiable.
- [Enneagram](https://www.enneagraminstitute.com/type-descriptions) - Type 6, [The Loyalist](https://www.enneagraminstitute.com/type-6). At their Best: internally stable and self-reliant, courageously championing themselves and others. Excellent "troubleshooters," they foresee problems and foster cooperation. They want to have security, to feel supported by others, to have certitude and reassurance, to test the attitudes of others toward them, to fight against anxiety and insecurity.
  
### Growth and Self-Awareness

I believe in continuous improvement and transparent leadership. Here are some key areas where I actively focus my growth:

- Cultivating meaningful feedback loops and communication
- Balancing comprehensive context with clear, actionable insights
- Channeling my detail-oriented nature into high-impact outcomes
- Sharing my thought process openly to keep team members aligned
- Building strong collaborative relationships and delegating effectively
- Leveraging my ADHD as a strength - it drives creative problem-solving and helps me forge unique connections
- Contributing enthusiastically to technical discussions while being mindful of team dynamics
- Bringing energy and engagement to team interactions, especially in our remote environment

## Exemplary READMEs

There are better manager READMEs than this one. I've found inspiration from the following READMEs:

- https://docs.google.com/presentation/d/1df5MALZKZU6lOeIXUiO-h6ReFM3KuIpnapSE97IZnX4/edit#slide=id.g2cf5bbf228_0_477
- https://drive.google.com/file/d/1mxUR69VBVBGPy-rq13jsgsgr67HosZOx/view?ref=hackernoon.com
- http://randsinrepose.com/archives/how-to-rands/
- https://gitlab.com/rayana/readme/-/blob/master/README.md
- https://gitlab.com/dcroft/dcroft
- https://gitlab.com/oregand/oregand/-/blob/main/hug.md

## Arguments against manager READMEs

I tend not to like manager READMEs, but I [wrote one anyway](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree).

- [I hate manager READMEs](https://medium.com/@skamille/i-hate-manager-readmes-20a0dd9a70d0)
- [Why I couldn't write a manager README](https://www.theengineeringmanager.com/growth/why-i-couldnt-write-a-manager-readme/)


## Footnotes

[^1]: [Just enough](https://www.agilenative.com/2019/12/just-enough/): the right amount, of the right kind, at the right time.
